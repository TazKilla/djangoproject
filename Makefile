-include .env
export

COLOR=\033[0;32m
NC=\033[0m
BOLD=\033[1m

COMPOSE_CMD=docker-compose -f docker-compose.yml
DJANGO_SERVICE=app

##
## ---------------------
## Available make target
## ---------------------
##

all: help
help:
	@grep -E '(^[a-zA-Z0-9_-.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## Provisioning
## ------------
##

init: ## Init .env files with default development values
	cp -i .env.dev .env
	sed -i 's/<DOCKER_USER>/$(shell echo $$USER)/g' .env
	sed -i 's/<DOCKER_USER_ID>/$(shell id -u $$USER)/g' .env
	@echo "${COLOR}[INFOS]${NC} New '.env' file generated from default development values"

build: ## Build Docker images and ensure they are up to date
	${COMPOSE_CMD} build

##
## Docker
## ------
##

start: ## Start containers
	${COMPOSE_CMD} up -d
	@${COMPOSE_CMD} ps

stop: ## Stop containers
	${COMPOSE_CMD} down --remove-orphans

restart: stop start ## Restart containers

logs: ## Display running containers logs (Press "Ctrl + c" to exit)
	@${COMPOSE_CMD} logs -f

ssh: ## Start new bash terminal inside the django container
	${COMPOSE_CMD} run --rm ${DJANGO_SERVICE} bash

##
## Infra
## -----
##

ansible.local: ## Run Ansible to install projects requirments (once)
	@ansible-playbook .ansible/project-setup.yml -v -K

db.install: ## Run sql migration to init the database
	${COMPOSE_CMD} run --rm ${DJANGO_SERVICE} python manage.py migrate

db.connect: ## Run bash terminal inside mysql container
	${COMPOSE_CMD} run --rm db /bin/bash -c 'mysql -h db -u $$MYSQL_USER -p$$MYSQL_PASSWORD $$MYSQL_DATABASE'
