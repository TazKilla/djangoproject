# ~/.bashrc: executed by bash(1) for non-login shells.

# Function to get current git branch
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Custom PS1 to avoid mistakes on other env
export PS1="\[\e[0;37m\]\u\[\e[0;37m\]@\[\e[0;37m\]docker \[\e[0;32m\]\w\[\e[0;33m\]$(parse_git_branch) \[\e[0;37m\]$ \[\e[0m\]"

#########
# ALIAS #
#########

alias ll='ls -alh'

# Some alias to avoid making mistakes
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Git
alias gs='git status'
alias gb='git branch'
alias gba='git branch --all'
alias gl='git log'
alias gls='git log --stat --summary'
alias gfo='git fetch origin --prune'
