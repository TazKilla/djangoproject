## Django example project with Docker & compose

### Run the demo :

    make init
    make build
    make start
    make db.install

Visit http://localhost

### Display available make targets :

    make help
